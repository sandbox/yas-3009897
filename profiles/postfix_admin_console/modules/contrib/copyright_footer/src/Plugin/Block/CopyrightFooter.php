<?php

// Updated by yas 2018/10/28
// Updated by yas 2018/01/30
// Updated by yas 2018/01/29
// Updated by yas 2016/08/28

/**
 * @file
 * Contains \Drupal\copyright_footer\Plugin\Block\CopyrightFooter.
 */
namespace Drupal\copyright_footer\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * @Block (
 *   id = "copyright_footer",
 *   admin_label = @Translation("Copyright Footer"),
 *   category = @Translation("Custom")
 * )
 */ 
class CopyrightFooter extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {

    return [
      'organization_name' => '',
      'year_origin' => '',
      'year_to_date' => '',
      'version' => '',
      'version_url' => '',
      'label_display' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $form['organization_name'] = [
      '#type' => 'textfield',
      '#title' => t('Organization name'),
      '#default_value' => $this->t($this->configuration['organization_name']),
    ];

    $form['year_origin'] = [
      '#type' => 'textfield',
      '#title' => t('Year orign from'),
      '#description' => t('Leave blank if not necessary.'),
      '#default_value' => $this->configuration['year_origin'],
    ];

    $date = new \DateTime();
    $form['year_to_date'] = [
      '#type' => 'textfield',
      '#title' => t('Year to date'),
      '#description' => t('Leave blank then the current year (@year) automatically shows up.', ['@year' => $date->format('Y')]),
      '#default_value' => $this->configuration['year_to_date'],
    ];

    $form['version'] = [
      '#type' => 'textfield',
      '#title' => t('Version'),
      '#description' => t('Leave blank if not necessary.'),
      '#default_value' => $this->configuration['version'],
    ];

    $form['version_url'] = [
      '#type' => 'textfield',
      '#title' => t('Version URL'),
      '#description' => t('Leave blank if not necessary.  It works w/ the version number above.  If you don\'t input the version number, this field will be simply ignored.'),
      '#default_value' => $this->configuration['version_url'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['organization_name'] = $form_state->getValue('organization_name');
    $this->configuration['year_origin']       = $form_state->getValue('year_origin');
    $this->configuration['year_to_date']      = $form_state->getValue('year_to_date');
    $this->configuration['version']           = $form_state->getValue('version');
    $this->configuration['version_url']       = $form_state->getValue('version_url');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $date = new \DateTime();

    $year_to_date = empty($this->configuration['year_to_date'])
                  ? $date->format('Y')
                  : $this->configuration['year_to_date'];

    $version = '';
    if ($this->configuration['version']
    &&  $this->configuration['version_url']) {
      $url = Url::fromUri($this->configuration['version_url']);
      $version =t('ver.@version', ['@version' => \Drupal::l($this->configuration['version'], $url)]);
    }


    return $this->configuration['year_origin'] == 0 || $date->format('Y') == $this->configuration['year_origin']
    ? ['#type' => 'markup',
       '#markup' => $this->t('Copyright &copy; @year @organization @version', [
        '@year' => $date->format('Y'),
        '@organization' => $this->configuration['organization_name'],
        '@version' => $version,
      ]),
    ]
    : ['#type' => 'markup',
      '#markup' => $this->t('Copyright &copy; @year_origin-@year_to_date @organization @version', [
        '@year_origin' => $this->configuration['year_origin'],
        '@year_to_date' => $year_to_date,
        '@organization' => $this->configuration['organization_name'],
        '@version' => $version,
      ]),
    ];
  }
}
